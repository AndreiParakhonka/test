package com.company.list;

import java.util.ArrayList;

/**
 * Created by Андрей on 25.06.2015.
 */
public class ArrayLisl implements MyList {

     private int size;
     private int callBusy = 0;
     private String[] array;
     private String[] temp;

    public ArrayLisl()
    {
        this(10);
    }

    public ArrayLisl(int size){
        this.size = size;
        array = new String [size];
    }

    @Override
    public String get(int i) {
        try {
            return array[i];
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("нет элемента ");
            return null;
        }
    }


    @Override
    public int size() {
        int a = 0;
        for(int i=0; i<size; i++){
            if(!(array[i] == null))
                a++;
        }
        return a;
    }

    @Override
    public void add(String element) {

        if(callBusy == size){
            int temp1;
            temp1 = size;
            size = (int) (size * 1.5);
            temp = new String[size];
            System.arraycopy(array, 0, temp,0, temp1  );
            array = temp;

        }
        array[callBusy++] = element;

    }

    @Override
    public boolean remove(String element) {
        try {
            for (int i=0; i<callBusy; i++){
                if(array[i].equals(element) ){

                    for (; i<callBusy ; i++){
                        array[i] = array[i+1];
                    }
                    array[callBusy ] = null;
                    callBusy--;
                    return true;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
        return false;
    }

    public void show() {
        for(int i=0; i<callBusy; i++){
            System.out.print(array[i] + " " );
        }
        System.out.println();
        for(int i=0; i<size; i++){
            System.out.print(array[i] + " " );
        }
    }
}
