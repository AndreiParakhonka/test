package com.company.list;

/**
 * Created by Андрей on 25.06.2015.
 */
public interface MyList {
    void add (String element); // добавляет элемент в список
    boolean remove (String element); // удаляет элемет
    int size();// возвращает размер;
    String get(int i); // элемен по индексу.
    void show();
}
