package test;

import com.company.list.ArrayLisl;
import com.company.list.MyList;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayLislTest {

    private MyList myList = new ArrayLisl();

    @Test
    public void testGet() throws Exception {
        myList.add("1");
        assertNotNull(myList.get(0));
    }

    @Test
    public void testSize() throws Exception {


        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.add("4");
        myList.add("5");
        myList.add("6");
        myList.add("7");
        myList.add("8");
        myList.add("9");
        myList.add("10");
        myList.add("11");
      //  assertNotNull(myList.get(10));
        assertEquals(11, myList.size());
        myList.show();
    }

    @Test
    public void testAdd() throws Exception {

    }

    @Test
    public void testRemove() throws Exception {
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.add("4");
        myList.add("5");
        myList.add("6");
        myList.add("7");
        myList.add("8");
        myList.add("1");
        myList.add("2");
        myList.add("3");
        myList.add("4");
        myList.add("5");
        myList.add("6");
        myList.add("7");
        myList.add("8");
       // assertArrayEquals();

        myList.show();

    }
}